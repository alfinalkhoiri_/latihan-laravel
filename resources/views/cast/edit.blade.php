@extends('layout.master')

@section('judul')
    Detail Pemain Baru
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
        <label>Nama : </label><br><br>
            <input type="text" name="nama" value="{{$cast->nama}}"><br><br>
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <label>Umur : </label><br><br>
            <input type="text" name="umur" value="{{$cast->umur}}"><br><br>
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <label>Bio</label><br><br>
            <textarea name="bio" id="" cols="20" rows="10">{{$cast->bio}}"</textarea><br>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <input type="submit" value="Submit">
    </form>

@endsection