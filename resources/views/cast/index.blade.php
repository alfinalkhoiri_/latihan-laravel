@extends('layout.master')

@section('judul')
    Tabel Pemain Film
@endsection

@section('content')

<a href="/cast/create" class="btn btn-secondary mb-2">Tambah Pemain Film</a>

<table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <h1>Data Tidak Ditemukan </h1>
    @endforelse


    </tbody>
  </table>
</div>


@endsection