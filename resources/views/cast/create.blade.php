@extends('layout.master')

@section('judul')
    Halaman Form Pemain Film Baru
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
        <label>Nama : </label><br><br>
            <input type="text" name="nama"><br><br>
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <label>Umur : </label><br><br>
            <input type="text" name="umur"><br><br>
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <label>Bio</label><br><br>
            <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <input type="submit" value="Sign Up">
    </form>

@endsection