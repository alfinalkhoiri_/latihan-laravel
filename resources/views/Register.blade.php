@extends('layout.master')

@section('judul')
    Halaman Form
@endsection

@section('content')
    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="get">
    @csrf
        <label>First name :</label><br><br>
            <input type="text" name="namadepan"><br><br>
        <label>Last name :</label><br><br>
            <input type="text" name="namabelakang"><br><br>
        <label>Gender</label><br><br>
            <input type="radio" name="gender">Male<br>
            <input type="radio" name="gender">Female<br><br>
        <label>Nationality</label><br><br>
            <select name="negara" id="">
                <option value="1">Indonesia</option>
                <option value="2">Inggris</option>
                <option value="3">Rusia</option>
            </select><br><br>
        <label>Language Spoken</label><br><br>
            <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
            <input type="checkbox" name="bahasa">English<br>
            <input type="checkbox" name="bahasa">Other<br><br>
        <label>Bio</label><br><br>
            <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection