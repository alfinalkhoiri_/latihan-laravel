@extends('layout.master')

@section('judul')
    Halaman Home
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$namadep}} {{$namabel}}</h1>
    <h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h3>
@endsection