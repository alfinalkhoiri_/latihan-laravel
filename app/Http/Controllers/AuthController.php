<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function data(){
        return view('Register');
    }
    public function beranda(Request $request){
        $namadep = $request['namadepan'];
        $namabel = $request['namabelakang'];
        return view('Welcome', compact('namadep', 'namabel'));
    }
}
