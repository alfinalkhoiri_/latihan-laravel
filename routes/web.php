<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/form', 'AuthController@data');

Route::get('/welcome', 'AuthController@beranda');

Route::get('/data-table', function(){
    return view('table.data-table');
});
//Route::get('/data-tables', 'IndexController@table');

// CRUD cast
// Create
Route::get('/cast/create', 'castController@create'); //menampilkan form untuk membuat data pemain film baru
Route::post('/cast', 'castController@store'); // menyimpan data baru ke tabel Cast

// Read
Route::get('/cast', 'castController@index'); //menampilkan list data para pemain film 
Route::get('/cast/{cast_id}', 'castController@show'); //menampilkan detail data pemain film dengan id tertentu

//Update
Route::get('/cast/{cast_id}/edit', 'castController@edit');// menampilkan form untuk edit pemain film dengan id tertentu
Route::put('/cast/{cast_id}', 'castController@update'); //menyimpan perubahan data pemain film (update) untuk id tertentu

//Delete
Route::delete('/cast/{cast_id}', 'castController@destroy'); //menghapus data pemain film dengan id tertentu